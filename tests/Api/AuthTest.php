<?php

namespace Tests\Api;

use App\User;
use Laravel\Passport\Passport;
use Tests\ApiTestCase;

class AuthTest extends ApiTestCase
{
    /**
     * User register test
     *
     * @dataProvider getRegisterDataProvider
     *
     * @param $body
     * @param $status
     */
    public function testUserRegister($body, $status)
    {
        $response = $this->post('/api/register', $body);

        $response->assertStatus($status);
        if ($status == self::STATUS_SUCCESS) {
            $response->assertJsonStructure(['success' => ["token", "name"]]);
        }
    }

    /**
     * @return array
     */
    public function getRegisterDataProvider()
    {
        return [
            [
                [
                    'name' => 'John',
                    'email' => 'example_email@example.com',
                    'password' => '123456',
                    'c_password' => '123456',
                ],
                self::STATUS_SUCCESS,
            ],
            [
                [
                    'name' => 'Ricky',
                    'email' => 'example_email',
                    'password' => '123456',
                    'c_password' => '123456',
                ],
                self::STATUS_UNAUTHORIZED,
            ],
            [
                [
                    'email' => 'example_email@example.com',
                    'password' => '123456',
                ],
                self::STATUS_UNAUTHORIZED,
            ],
            [
                [
                    'name' => 'Nina',
                    'email' => 'example_email@example.com',
                    'password' => '123456',
                    'c_password' => '6543221',
                ],
                self::STATUS_UNAUTHORIZED,
            ],
        ];
    }

    /**
     * User login test
     *
     * @dataProvider getLoginDataProvider
     *
     * @param $body
     * @param $status
     */
    public function testUserLogin($body, $status)
    {
        $response = $this->post('/api/login', $body);
        $response->assertStatus($status);
        if ($status == self::STATUS_SUCCESS) {
            $response->assertJsonStructure(['success' => ["token", "name"]]);
        }
    }

    /**
     * @return array
     */
    public function getLoginDataProvider()
    {
        return [
            [
                [
                    'email' => self::TEST_USER_EMAIL,
                    'password' => self::TEST_USER_PASSWORD,
                ],
                self::STATUS_SUCCESS,
            ],
            [
                [
                    'email' => 'example_email',
                    'password' => '123456',
                ],
                self::STATUS_UNAUTHORIZED,
            ],
        ];
    }


    /**
     * User details test
     *
     * @dataProvider getDetailsDataProvider
     *
     * @param $email
     * @param $status
     */
    public function testUserDetails($email, $status)
    {
        Passport::actingAs(User::where("email", $email)->first());
        $response = $this->get('/api/details');
        $response->assertStatus($status);
        if ($status == self::STATUS_SUCCESS) {
            $response->assertJsonStructure(['success' => ["id", "name", "email", "email_verified_at", "created_at"]]);
        }
    }

    /**
     * @return array
     */
    public function getDetailsDataProvider()
    {
        return [
            [
                self::TEST_USER_EMAIL,
                self::STATUS_SUCCESS,
            ],
        ];
    }
}
