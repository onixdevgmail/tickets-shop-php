<?php

namespace Tests\Api;

use App\User;
use Laravel\Passport\Passport;
use Tests\ApiTestCase;

class EventTest extends ApiTestCase
{
    /**
     * Get events list test
     *
     * @dataProvider getEventDataProvider
     *
     * @param $email
     * @param $item_count
     * @param $status
     */
    public function testEventList($email, $item_count, $status)
    {
        Passport::actingAs(User::where("email", $email)->first());
        $response = $this->get('/api/events');
        $response->assertStatus($status);
        if ($status == self::STATUS_SUCCESS) {
            $response->assertJsonStructure(self::EVENTS_LIST_FIELD_ARRAY)->assertJsonCount($item_count, 'data');
        }
    }

    /**
     * @return array
     */
    public function getEventDataProvider()
    {
        return [
            [
                self::TEST_USER_EMAIL,
                10,
                self::STATUS_SUCCESS,
            ],
        ];
    }

    /**
     * Add to cart test
     *
     * @dataProvider getAddToCartDataProvider
     *
     * @param $email
     * @param $event_id
     * @param $quantity
     * @param $status
     */
    public function testAddToCart($email, $event_id, $quantity, $status)
    {
        Passport::actingAs(User::where("email", $email)->first());
        $response = $this->post(route('add-to-cart', $event_id), ["quantity" => $quantity]);
        $response->assertStatus($status);
        if ($status == self::STATUS_SUCCESS) {
            $response->assertJsonStructure(self::ADD_TO_CART_FIELD_ARRAY);
        }
    }

    /**
     * @return array
     */
    public function getAddToCartDataProvider()
    {
        return [
            [
                self::TEST_USER_EMAIL,
                self::EVENT_ID_3,
                1,
                self::STATUS_SUCCESS,
            ],
            [
                self::TEST_USER_EMAIL,
                rand(100, 200),
                1,
                self::STATUS_NOT_FOUND,
            ],
        ];
    }
}
