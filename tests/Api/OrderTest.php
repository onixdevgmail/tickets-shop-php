<?php

namespace Tests\Api;

use App\User;
use Laravel\Passport\Passport;
use Tests\ApiTestCase;

class OrderTest extends ApiTestCase
{
    /**
     * Get current order info test
     *
     * @dataProvider getOrderDataProvider
     *
     * @param $email
     * @param $events
     * @param $status
     */
    public function testGetOrder($email, $events, $status)
    {
        Passport::actingAs(User::where("email", $email)->first());
        if ($events) {
            foreach ($events as $event) {
                $this->post(route('add-to-cart', $event), ["quantity" => 1]);
            }
        }
        $response = $this->get(route("order"));
        $response->assertStatus($status);
        if ($status == self::STATUS_SUCCESS) {
            $response->assertJsonStructure(self::ORDER_FIELD_ARRAY);
            if ($events) {
                $response->assertJsonCount(count($events), 'items');
            }
        }
    }

    /**
     * @return array
     */
    public function getOrderDataProvider()
    {
        return [
            [
                self::TEST_USER_EMAIL,
                [self::EVENT_ID_1, self::EVENT_ID_5],
                self::STATUS_SUCCESS,
            ],
            [
                self::TEST_USER_EMAIL_123,
                [self::EVENT_ID_2],
                self::STATUS_SUCCESS,
            ],
        ];
    }


    /**
     * @dataProvider getPayOrderDataProvider
     *
     * @param $email
     * @param $events
     * @param $token
     * @param $success
     * @param $status
     */
    public function testPayOrder($email, $events, $token, $success, $status)
    {
        Passport::actingAs(User::where("email", $email)->first());
        if ($events) {
            foreach ($events as $event) {
                $this->post(route('add-to-cart', $event), ["quantity" => 1]);
            }
        }
        $response = $this->post(route("order-pay"), ["token" => $token]);
        $response->assertStatus($status);
        if ($status == self::STATUS_SUCCESS) {
            $response->assertJsonStructure(self::ORDER_PAY_FIELD_ARRAY)->assertJson(['success' => $success]);
        }
    }

    public function getPayOrderDataProvider()
    {
        return [
            [
                self::TEST_USER_EMAIL,
                [self::EVENT_ID_4, self::EVENT_ID_2],
                "tok_visa",
                true,
                200,
            ],
            [
                self::TEST_USER_EMAIL_123,
                [self::EVENT_ID_3],
                "tok_visa",
                true,
                200,
            ],
            [
                self::TEST_USER_EMAIL,
                [],
                "tok_visa",
                false,
                200,
            ],
            [
                self::TEST_USER_EMAIL,
                [self::EVENT_ID_4, self::EVENT_ID_2],
                null,
                false,
                302,
            ],
            [
                self::TEST_USER_EMAIL_123,
                [self::EVENT_ID_2, self::EVENT_ID_3, self::EVENT_ID_4],
                "tok_cvcCheckFail",
                false,
                200,
            ],
        ];
    }

}
