<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;

class ApiTestCase extends TestCase
{

    use DatabaseTransactions;

    protected $headers = ['Accept' => 'application/json'];

    const STATUS_SUCCESS = 200;
    const STATUS_UNAUTHORIZED = 401;
    const STATUS_NOT_FOUND = 404;

    const TEST_USER_EMAIL = "test_email@example.com";
    const TEST_USER_EMAIL_123 = "test_email123@example.com";
    const TEST_USER_PASSWORD = "43NwBADv";

    const EVENT_ID_1 = 1;
    const EVENT_ID_2 = 2;
    const EVENT_ID_3 = 3;
    const EVENT_ID_4 = 4;
    const EVENT_ID_5 = 5;

    const ORDER_FIELD_ARRAY = [
        'id',
        'status',
        'transaction_id',
        'total_price',
        'created_at',
        'items' => [
            "*" => self::ORDER_ITEM_FIELD_ARRAY,
        ],
    ];

    const ORDER_PAY_FIELD_ARRAY = [
        'success',
        'message',
    ];


    const ORDER_ITEM_FIELD_ARRAY = [
        'id',
        'quantity',
        'event_id',
        'created_at',
        'event' => self::EVENT_FIELD_ARRAY,
    ];
    const EVENT_FIELD_ARRAY = [
        'id',
        'name',
        'price',
    ];

    const EVENTS_LIST_FIELD_ARRAY = [
        'data' => [
            "*" => self::EVENT_FIELD_ARRAY,
        ],
        'current_page',
        'next_page_url',
        'prev_page_url',
        'total',

    ];
    const ADD_TO_CART_FIELD_ARRAY = [
        'success',
        'order' => self::ORDER_FIELD_ARRAY,
    ];

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate');
        Artisan::call('passport:install');
        Artisan::call('db:seed --class=EventSeeder');
        factory(User::class)->create([
            "email" => self::TEST_USER_EMAIL,
            "password" => bcrypt(self::TEST_USER_PASSWORD),
        ]);
        factory(User::class)->create([
            "email" => self::TEST_USER_EMAIL_123,
            "password" => bcrypt(self::TEST_USER_PASSWORD),
        ]);
    }

    public function get($uri, array $headers = [])
    {
        return parent::get($uri, array_merge($this->headers, $headers));
    }

    public function post($uri, array $headers = [])
    {
        return parent::post($uri, array_merge($this->headers, $headers));
    }

}
