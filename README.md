# Instruction

## Windows users:
- Download wamp: http://www.wampserver.com/en/
- Download and extract cmder mini: https://github.com/cmderdev/cmder/releases/download/v1.1.4.1/cmder_mini.zip
- Update windows environment variable path to point to your php install folder (inside wamp installation dir) (here is how you can do this http://stackoverflow.com/questions/17727436/how-to-properly-set-php-environment-variable-to-run-commands-in-git-bash)
 

## Mac Os, Ubuntu and windows users continue here:
- Create a database locally named
- Open the console and cd your project root directory
- Rename `.env.example` file to `.env` inside your project root and fill the database information.
  (windows wont let you do it, so you have to open your console cd your project root directory and run `mv .env.example .env` )
- Run `composer install` or ```php composer.phar install```
- Run `php artisan key:generate` 
- Run `php artisan migrate`
- Run `php artisan db:seed` to run seeders, if any.
- Run `php artisan db:seed --class=EventSeeder` to run Events seeders, if needed.
- Run `php artisan serve`

##### You can now access your project at localhost:8000 :)

## If for some reason your project stop working do these:
- `composer install`
- `php artisan migrate`


## Run Tests:
- `./vendor/bin/phpunit`
- `./vendor/bin/phpunit --filter 'Api'` - for API Tests


## API Documentation
- `https://documenter.getpostman.com/view/8238993/SzmY9M9d`
