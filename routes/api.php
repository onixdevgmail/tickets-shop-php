<?php

use Illuminate\Support\Facades\Route;

Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('details', 'Api\UserController@details');
    Route::get('events', 'Api\EventController@list');
    Route::post('events/{event}/add-to-cart', 'Api\EventController@add_to_cart')->name("add-to-cart");
    Route::get('order', 'Api\OrderController@order')->name("order");
    Route::post('order/pay', 'Api\OrderController@order_pay')->name("order-pay");
});

