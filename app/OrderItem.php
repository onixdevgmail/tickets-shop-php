<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    /**
     * @var array
     */
    protected $fillable = ["event_id"];

    /**
     * @var array
     */
    protected $hidden = ["order_id", "updated_at"];

    /**
     * @var array
     */
    protected $with = ["event"];


    /**
     * Get order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }


    public function event()
    {
        return $this->belongsTo('App\Event', 'event_id');
    }
}
