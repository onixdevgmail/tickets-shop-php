<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const STATUS_UNKNOWN = 0;
    const STATUS_PENDING = 1;
    const STATUS_PAYED = 2;


    /**
     * @var array
     */
    protected $fillable = ["user_id", "status", "transaction_id"];


    /**
     * @var array
     */
    protected $hidden = [
        'user_id',
        'updated_at',
    ];


    /**
     * @var array
     */
    protected $appends = ["total_price"];

    /**
     * @var array
     */
    protected $with = ["items"];


    /**
     * @return int
     */
    public function getTotalPriceAttribute()
    {
        $value = 0;
        foreach ($this->items()->get() as $item) {
            $value += $item->quantity * $item->event->price;
        }
        return $value;
    }


    /**
     * Get order items
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('App\OrderItem', 'order_id');
    }


    /**
     *
     * Get user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
