<?php

namespace App\Http\Controllers\API;

use App\Helpers\OrderHelper;
use App\Http\Requests\OrderPayRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * Get current order for user
     *
     * @return JsonResponse
     */
    public function order()
    {
        return response()->json(Auth::user()->currentOrder());
    }

    /**
     * Order pay
     *
     * @param OrderPayRequest $request
     *
     * @return JsonResponse
     */
    public function order_pay(OrderPayRequest $request)
    {
        return OrderHelper::order_pay($request->get("token"));
    }
}
