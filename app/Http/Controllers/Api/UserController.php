<?php

namespace App\Http\Controllers\API;

use App\Helpers\UserHelper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Login api method
     *
     * @return JsonResponse
     */
    public function login()
    {
        return UserHelper::user_login();
    }

    /**
     * Register api method
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        return UserHelper::user_register($request);
    }

    /**
     * Details api method
     *
     * @return JsonResponse
     */
    public function details()
    {
        return UserHelper::user_detail();
    }
}
