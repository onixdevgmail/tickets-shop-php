<?php

namespace App\Http\Controllers\API;

use App\Event;
use App\Helpers\EventHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddToCartRequest;
use Illuminate\Http\JsonResponse;

class EventController extends Controller
{
    /**
     * Get list of events
     *
     * @return JsonResponse
     */
    public function list()
    {
        return response()->json(Event::paginate(10));
    }


    /**
     * Add item to user order
     *
     * @param AddToCartRequest $request
     * @param Event            $event
     *
     * @return JsonResponse
     */
    public function add_to_cart(AddToCartRequest $request, Event $event)
    {
        return EventHelper::add_to_cart($event, $request->get("quantity"));
    }
}
