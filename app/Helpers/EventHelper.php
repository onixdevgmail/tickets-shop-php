<?php


namespace App\Helpers;


use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class EventHelper
{
    /**
     * Helper for add item to cart
     *
     * @param $event
     * @param $quantity
     *
     * @return JsonResponse
     */
    static function add_to_cart($event, $quantity)
    {
        $order = Auth::user()->currentOrder();
        $item = $order->items()->firstOrNew(["event_id" => $event->id]);
        $item->quantity += $quantity;
        $item->save();

        return response()->json(["success" => true, "order" => $order->fresh()]);
    }
}
