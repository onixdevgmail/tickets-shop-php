<?php


namespace App\Helpers;


use App\Order;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Stripe\Stripe;

class OrderHelper
{

    /**
     * Helper for order pay
     *
     * @param $token
     *
     * @return JsonResponse
     */
    static function order_pay($token)
    {
        $order = Auth::user()->currentOrder();
        $success = $message = true;
        Stripe::setApiKey(env("STRIPE_SECRET_KEY"));
        if ($order->total_price > 1) {
            try {
                $charge = \Stripe\Charge::create([
                    'amount' => $order->total_price * 100,
                    'currency' => 'usd',
                    'description' => "Order #{$order->id} pay",
                    'source' => $token,
                ]);
                if ($charge['status'] == 'succeeded') {
                    $message = "Payment successfully";
                    $order->status = Order::STATUS_PAYED;
                    $order->transaction_id = $charge->id;
                    $order->save();
                }
            } catch (\Exception $e) {
                $success = false;
                $message = "Something went wrong. Please try again ({$e->getMessage()})";
                Log::error($e->getMessage());
            }
        } else {
            $success = false;
            $message = "You don't have items in order.";
        }

        return response()->json(["success" => $success, "message" => $message]);
    }
}
