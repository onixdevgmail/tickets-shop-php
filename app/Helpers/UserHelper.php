<?php


namespace App\Helpers;


use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;

class UserHelper
{
    /**
     * Helper for login user end return token
     *
     * @return JsonResponse
     */
    static function user_login()
    {
        if (Auth::attempt(['email' => Request::input('email'), 'password' => Request::input('password')])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['name'] = $user->name;

            return response()->json(['success' => $success], 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }


    /**
     * Helper for user register
     *
     * @param $request
     *
     * @return JsonResponse
     */
    static function user_register($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->name;

        return response()->json(['success' => $success], 200);
    }

    /**
     * Helper for get user details
     *
     * @return JsonResponse
     */
    static function user_detail()
    {
        return response()->json(['success' => Auth::user()], 200);
    }

}
